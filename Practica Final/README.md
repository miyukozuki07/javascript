# Curso Javascript
### Este repositorio contiene la práctica final del curso Javascript de la beca de capacitación en Ingeniería de Software.

* Autor: Avila Reyes Paola de los Angeles
* Contacto: candyrini96@live.com.mx 

Practica Final de Javascript
Esta practica consiste en dar funcionalidad al buscador de la pagina proporcionada por el profesor a cargo del curso.
Para intentar dar funcionalidad al buscador, ocupando la API del sitio web the movie DB, se extrae la información de su catalogo de películas que estuvieran relacionadas con la búsqueda que haría el usuario final al consultar. La palabra ingresada en el buscador sera guardada en una variable de nombre palabra, la cual indicara (a través de una función)cual son las posibles coincidencias en los títulos correspondientes a las peliculas que se encuentren en la base de datos. 
Estas coincidencias tienen que mostrarse en el apartado correspondiente <div> dentro de la pagina. 

En esta práctica dejo las modificaciones que realice en el archivo javascript buscar.js
Sin embargo, no pude hacer que se vinculara la base de datos The movie DB a la página a pesar de seguir el código que aparece en los archivos index.js y catalogo.js.
Pensé que el error estaba en la API key, por lo que utilice la API Key que venia en el código de catalogo.js
Aunque al hacer unas pruebas en el navegador y revisar si la pagina hacia la petición a través de la API esta no aparecía, entonces creo que el error esta en el código de javascript. 
